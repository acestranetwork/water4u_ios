var app_id = 'WO_CFoA_O1tb23feBFTsK8u-r';
var model = {};
var device_token = '';
var backbutton = 0;
var set = {};
var get_token = '';
var push = '';
var interval = '';
var xpay_id = '';
var firebase_config = {};
/*
 var url = 'http://water4u/water_api/index.php';
 var url_new = 'http://water4u/api/';*/
 var url = 'http://water4u.lk/water_api/index.php';
 var url_new = 'http://water4u.lk/api/';
 var api = 'http://backup.acecommunicate.com/watermail.php';
 var curency = 'Rs.';
 var token_new = 'b8335f62-70b4-4548-8d75-6a7e809e0c39';
 var token = token_new;
 var app_user = {};
 app_user = JSON.parse(localStorage.getItem('water_app_user'));

 angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'firebase'])

 .run(function($ionicPlatform, $ionicHistory, $state, mylaipush, $cordovaToast,service,$rootScope,$ionicLoading,$timeout) {
  $ionicPlatform.ready(function() {
   service.post(url_new+'firebase_config',{token:token}).then(function(res){
    firebase_config = res.data;
    firebase.initializeApp(firebase_config);
  })
      ionic.Platform.fullScreen();
   if (window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    cordova.plugins.Keyboard.disableScroll(true);
  }
  // if (window.StatusBar) {
  //   StatusBar.styleDefault();
  // }
  if (window.Connection) {
    if (navigator.connection.type == Connection.NONE) {
      $cordovaToast.showLongBottom('Sorry, No Internet connectivity or server error. Please reconnect and try again.');
    }
  }
  $rootScope.$on('loading:show', function() {

    spinner='<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>';
    $ionicLoading.show({  template: spinner,
        duration: 2000,animation: 'fadeIn',
              showDelay: 10})
  })

  $rootScope.$on('loading:hide', function() {
      $timeout(function () {
    $ionicLoading.hide();
  }, 1000);
  })
  $ionicPlatform.registerBackButtonAction(function(e) {
    if ($ionicHistory.currentStateName() == 'app.home_page' || $ionicHistory.currentStateName() == 'app.delivery_order_list') {
      ionic.Platform.exitApp();
    } else if ($ionicHistory.backView()) {
      $ionicHistory.backView().go();
    } else if ($ionicHistory.currentStateName() != 'app.home_page' && app_user.role == 'customer') {
      $state.go('app.home_page');
      $ionicHistory.removeBackView();
    } else if ($ionicHistory.currentStateName() != 'app.delivery_order_list' && app_user.role == 'DeliveryBoy') {
      $state.go('app.delivery_order_list');
      $ionicHistory.removeBackView();
    } else {
      ionic.Platform.exitApp();
    }
    return false;
  }, 101);
  var push = PushNotification.init({ "ios": { "alert": "true", "badge": "true", "sound": "true", "clearBadge": true } });
  push.on('notification', function(data) {
    $state.go('app.' + data.additionalData.openUrl);
  });
});
})

 .config(function($stateProvider, $urlRouterProvider, $cordovaInAppBrowserProvider, $httpProvider) {
  var defaultOptions = {
    location: 'no',
    clearcache: 'no',
    toolbar: 'no'
  };
  $httpProvider.interceptors.push(function($rootScope) {

 return {
   request: function(config) {
     $rootScope.$broadcast('loading:show')
     return config
   },
   response: function(response) {
     $rootScope.$broadcast('loading:hide')
     return response
   }
 }
})
  document.addEventListener("deviceready", function() {
    $cordovaInAppBrowserProvider.setDefaultOptions(options)
  }, false);
  $stateProvider
  .state('app', {
    url: '/app',
    cache: false,
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('first_page', {
    url: '/first_page',
    cache: false,
    templateUrl: 'templates/user/first_page.html',
    controller: 'homeCtrl'
  })
  .state('app.home_page', {
    url: '/home_page',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/home_page.html',
        controller: 'homeCtrl'
      }
    }
  })
  .state('app.about', {
    url: '/about',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/about_us.html'
      }
    }
  })
  .state('app.termsandcondition', {
    url: '/termsandcondition',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/terms_and_condition.html'
      }
    }
  })
  .state('app.products_list', {
    params: { brand: null },
    cache: false,
    url: '/products_list',
    views: {
      'menuContent': {
        templateUrl: 'templates/user/brand_list.html',
        controller: 'waterlistCtrl'
      }
    }
  })
  .state('app.cart', {
    params: { product_id: null },
    cache: false,
    url: '/cart',
    views: {
      'menuContent': {
        templateUrl: 'templates/user/cart.html',
        controller: 'cartCtrl'
      }
    }
  })
  .state('app.login', {
    url: '/login',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/login.html',
        controller: 'loginCtrl'
      }
    }
  })
  .state('app.create_user', {
    url: '/create_user',
    views: {
      'menuContent': {
        templateUrl: 'templates/user/create_user.html',
        controller: 'UserRegCntrl'
      }
    }
  })
  .state('app.enquiry', {
    url: '/enquiry',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/enquiry.html',
        controller: 'MyProfileCtrl'
      }
    }
  })
  .state('app.manage_address', {
    url: '/manage_address',
    cache: false,
    params: { data: null },
    views: {
      'menuContent': {
        templateUrl: 'templates/user/address_list.html',
        controller: 'manage_addressCtrl'
      }
    }
  })
  .state('app.checkout', {
    url: '/checkout',
    cache: false,
    params: { data: null },
    views: {
      'menuContent': {
        templateUrl: 'templates/user/checkout.html',
        controller: 'checkoutCtrl'
      }
    }
  })
  .state('app.payment', {
    url: '/payment',
    cache: false,
    params: { data: null },
    views: {
      'menuContent': {
        templateUrl: 'templates/user/payment.html',
        controller: 'paymentCtrl'
      }
    }
  })
  .state('app.forgot_password', {
    url: '/forgot_password',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/forgot_password.html',
        controller: 'forgotPasswordCtrl'
      }
    }
  })
  .state('app.my_profile', {
    url: '/my_profile',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/my_profile.html',
        controller: 'MyProfileCtrl'
      }
    }
  })
  .state('app.my_offer', {
    url: '/my_offer',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/my_offer.html',
        controller: 'MyOfferCtrl'
      }
    }
  })
  .state('app.my_order', {
    url: '/my_order',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/user/my_orders.html',
        controller: 'MyOrderCtrl'
      }
    }
  })
  .state('app.delivery_order_list', {
    url: '/delivery_order_list',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/delivery_boy/order_list.html',
        controller: 'delivery_boyCtrl'
      }
    }
  })
  .state('app.delivery_map', {
    url: '/delivery_map/:address',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/delivery_boy/map.html',
        controller: 'delivery_mapCtrl'
      }
    }
  })
  .state('app.order_details', {
    url: '/order_details/:order_id',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/delivery_boy/order_details.html',
        controller: 'order_detailsCtrl'
      }
    }
  })
  .state('app.order_history', {
    url: '/order_history',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/delivery_boy/order_history.html',
        controller: 'delivery_boyCtrl'
      }
    }
  });
  if (localStorage.getItem('water_app_user') != null) {
    data = JSON.parse(localStorage.getItem('water_app_user'));
    if (data.role == 'DeliveryBoy') {
      $urlRouterProvider.otherwise('/app/delivery_order_list');
    } else {
      $urlRouterProvider.otherwise('/app/home_page');
    }
  } else {
    $urlRouterProvider.otherwise('/app/home_page');
  }
})
