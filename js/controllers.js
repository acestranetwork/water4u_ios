angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $interval, $timeout, $state, service, $cordovaToast, $rootScope, mylaipush) {
    var obj = { 'token': token, 'page': 'get_brands' };
    service.post(url, obj).then(function(result) {
        $scope.brands = result.data;
    });
    $scope.status = false;
    $scope.show_count = false;
    var interval = '';
    app_user = JSON.parse(localStorage.getItem("water_app_user"));
    count_cart = function() {
        data = JSON.parse(localStorage.getItem("water_cart"));
        if (data && data.length > 0) {
            $scope.show_count = true;
            $scope.cart_count = data.length;
        } else {
            $scope.show_count = false;
        }
    }
    count_cart();
    $rootScope.check = function() {
        if (app_user && app_user.role != '') {
            $scope.role = app_user.role;
            $scope.user = app_user;
        }
    }
    $rootScope.check_track = function(status) {
        if (app_user != {} && status == true && app_user.role == "DeliveryBoy") {
            interval = $interval(track, 30000);
        } else {
            $interval.cancel(interval);
        }
    }
    $rootScope.check();
    $scope.logout = function() {
        $interval.cancel(interval);
        localStorage.removeItem('water_app_user');
        localStorage.removeItem('device_token')
        app_user = {};
        $scope.status = false;
        $scope.role = "";
        $state.go('first_page');
    };
    get_notification = function() {
        app_id = 'WO_CFoA_O1tb23feBFTsK8u-r';
        model = ionic.Platform.device();
        push = PushNotification.init({ "ios": { "alert": "true", "badge": "true", "sound": "true", "clearBadge": true } });
        push.on('registration', function(data) {
            device_token = data.registrationId;
            get_token = JSON.parse(localStorage.getItem('device_token'));
            if (get_token != device_token || !get_token || get_token == '' || get_token == null) {
                localStorage.setItem('device_token', JSON.stringify(device_token));
                var obj = { 'app_id': app_id, 'device_token': device_token, 'model': model.model, 'platform': model.platform, 'status': true }
                mylaipush.notification(obj);
            }
            notification_data = {};
            notification_data.token = token;
            notification_data.user_id = app_user.id;
            notification_data.device_token = device_token;
            service.post(url_new + 'device_register', notification_data).then(function(result) {})
        });
    }
})

.controller('UserRegCntrl', function($scope, $state, service, $cordovaToast, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,
    $ionicPopup, $ionicActionSheet, $ionicLoading) {
    $scope.reg_data = {};
    $scope.type = 'Residential';
    service.post(url_new + 'city_list', {token:token}).then(function(res) {
      $scope.cities = res.data;
  })
    $scope.UserReg = function(user, type) {
        if (user.password == user.confirm_password) {
            user.type = type;
            user.token = token;
            user.page = 'user_register';
            console.log(user);
            $ionicLoading.show({
                template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
            })
            if ($scope.model_image) {
                var targetPath = $scope.pathForImage($scope.model_image);
                user.image = $scope.model_image;
                var options = {
                    fileKey: "image",
                    fileName: $scope.model_image,
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params: user
                };
                $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
                    $ionicLoading.hide();
                    data = JSON.parse(result.response);
                    if (data.status == 'success') {
                        $cordovaToast.showLongCenter(data.message);
                        $scope.reg_data = {};
                        $state.go('app.login');
                    } else {
                        $cordovaToast.showLongCenter(data.message);
                    }
                }, function(error) {
                    $ionicLoading.hide();
                    $cordovaToast.showShortBottom("error" + JSON.stringify(error));

                });
            } else {
                service.post(url, user).then(function(result) {
                    $ionicLoading.hide();
                    $cordovaToast.showShortBottom(result.message);
                    $scope.reg_data = {};
                    $state.go('app.login');
                }, function(error) {
                    $cordovaToast.showShortBottom("error" + JSON.stringify(error));
                });
            }
        } else {

            $cordovaToast.showLongCenter('password mismatched');
        }
    }
    $scope.loadImage = function() {
        var showActionSheet = $ionicActionSheet.show({
            buttons: [{
                text: 'Load from Library'
            },
            {
                text: 'Use Camera'
            }
            ],
            titleText: 'Select Image Source',
            cancelText: 'Cancel',
            cancel: function() {
            },
            buttonClicked: function(index) {
                var type = null;
                if (index === 0) {
                    type = Camera.PictureSourceType.PHOTOLIBRARY;
                }
                if (index === 1) {
                    type = Camera.PictureSourceType.CAMERA;
                }
                if (type !== null) {
                    $scope.selectPicture(type);
                }
                return true;
            },
            destructiveButtonClicked: function() {
            }
        });
    };
    $scope.pathForImage = function(image) {
        if (image === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + image;
        }
    };
    var temp = {};
    $scope.selectPicture = function(sourceType) {
        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imagePath) {
            var currentName = imagePath.replace(/^.*[\\\/]/, '');
            var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
                window.FilePath.resolveNativePath(imagePath, function(entry) {
                    window.resolveLocalFileSystemURL(entry, success, fail);
                    function fail(e) {
                        $cordovaToast.showShortBottom('Error: 9', e);
                    }
                    function success(fileEntry) {
                        var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                        $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success) {
                            $scope.model_image = newFileName;
                        }, function(error) {
                            $cordovaToast.showShortBottom('Error : ' + error.exception);
                        });
                    };
                });
            } else {
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success) {
                    $scope.model_image = newFileName;
                }, function(error) {
                    $cordovaToast.showShortBottom('Error : ' + error.exception);
                });
            }
        },
        function(err) {
            $cordovaToast.showShortBottom('Error :12');
        })
    };
    $scope.camera_open = function($image) {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var image = document.getElementById('myImage');
            image.src = "data:image/jpeg;base64," + imageData;
        }, function(err) {
        });
    };
})

.controller('loginCtrl', function($scope, $stateParams, service, $state, $cordovaToast, $rootScope, $ionicLoading) {
    $scope.doLogin = function(data) {
        if (data) {
            data.token = token;
            data.page = 'login';
            $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
            service.post(url, data).then(function(result) {
                $ionicLoading.hide();
                if (result.status == 'success') {
                    app_user = result.data;
                    $scope.role = result.data.role;
                    localStorage.setItem('water_app_user', JSON.stringify(app_user));
                    $scope.role = result.data.role;
                    get_notification();
                    if ($scope.role == 'DeliveryBoy') {
                        if (app_user.attendance_status == 1) {
                            $scope.status = true;
                        } else {
                            $scope.status = false;
                        }
                        $state.go('app.delivery_order_list');
                    } else {

                        $state.go('app.home_page');
                    }
                    $rootScope.check();
                } else {
                    $cordovaToast.showShortBottom(result.message);
                }
            }, function(error) {
                if (error == 500 || error == 404 || error == 0) {
                    $cordovaToast.showShortBottom('Network Failed' + error);
                }
            });
        } else {
            $cordovaToast.showShortBottom('Please enter all the fields');
        }
    };
})

.controller('homeCtrl', function($scope, $stateParams, $state, $cordovaToast) {
    count_cart();
    $scope.go = function() {
        $state.go('app.home_page');
    }
    $scope.select_brand = function(brand) {
        $state.go('app.products_list', { brand: brand });
    }
})

.controller('manage_addressCtrl', function($scope, $stateParams, $state, $cordovaToast, service, $ionicLoading) {
    data = {};
    data.token = token;
    data.page = 'users_address';
    data.user_id = app_user.id;
    $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
    service.post(url, data).then(function(result) {
        $scope.explaining = result.data;
        $ionicLoading.hide();
    })
    service.post(url_new + 'city_list', {token:token}).then(function(res) {
      $scope.cities = res.data;
  })
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
    $scope.update = function(address_data) {
        address_data.token = token;
        address_data.page = 'update_address';
        address_data.customer_id = app_user.id;
        if (!address_data.type) {
            address_data.type = '';
        }
        service.post(url, address_data).then(function(result) {
            if (result.status == 'success') {
                $cordovaToast.showShortBottom('Address stored successfully');
            } else {
                $cordovaToast.showShortBottom('Please fill all the fileds');
            }
        })
    }
})

.controller('waterlistCtrl', function($scope, $stateParams, $state, service, $cordovaToast, $ionicLoading) {
    $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
    var brand_id = $stateParams.brand;
    var obj = { 'token': token, 'page': 'user_product', 'brand_id': brand_id };
    service.post(url, obj).then(function(result) {
        $ionicLoading.hide();
        console.log(result)
        if(result.data.list && result.data.list.length > 0)
        {
            $scope.products = result.data.list;
        }
        else
        {
            $scope.no_records = true;
        }
        $scope.brand_image = result.data.brand_image;
    }, function(error) {
        $cordovaToast.showShortBottom("error" + JSON.stringify(error));
    });
    $scope.goto_select_water = function(product_id) {
        $state.go('app.cart', { product_id: product_id });
    }
    var temp = [];
    var temp1 = [];
    $scope.add_cart = function(cart_data) {
        cart = [];
        check = [];
        cart = JSON.parse(localStorage.getItem("water_cart"));
        if (!cart) {
            cart = [];
            data = {};
            data.id = cart_data.id;
            data.quantity = 1;
            data.price = cart_data.price;
            data.stock = cart_data.stock;
            cart.push(data);
            $cordovaToast.showLongBottom('Product added successfully to your cart');

        } else {
            angular.forEach(cart, function(value, key) {
                if (value.id == cart_data.id) {
                    check.push(cart_data.id);
                }
            })
            if (check.length == 0) {
                data = {};
                data.id = cart_data.id;
                data.quantity = 1;
                data.price = cart_data.price;
                data.stock = cart_data.stock;
                cart.push(data);
                $cordovaToast.showShortBottom('Product added successfully to your cart');
            } else {
                $cordovaToast.showShortBottom("already in cart");
            }
        }
        localStorage.setItem('water_cart', JSON.stringify(cart));
        count_cart();
    }
})

.controller('cartCtrl', function($scope, $state, $stateParams, product_list_services, single_list_services, $ionicPopup, $ionicPopup, $cordovaToast, $ionicModal, service, $ionicLoading) {
    $scope.total;
    $scope.deliver_data = {};
    $scope.city = 'Dehiwala';
    service.post(url_new + 'city_list', {token:token}).then(function(res) {
      $scope.cities = res.data;
  })
    cart_list = function() {
        $scope.total = 0;
        cart_data = JSON.parse(localStorage.getItem("water_cart"));
        $scope.productlist = cart_data;
        cart_products = [];
        if (cart_data && cart_data.length > 0) {
            cart_data.forEach(function(value) {
                cart_products.push(value.id);
            })
            var obj = { 'token': token, 'page': 'cart_product_details', 'products_id': cart_products };
            service.post(url, obj).then(function(result) {
                if (result.status == 'success') {
                    angular.forEach(result.data, function(value, key) {
                        angular.forEach(cart_data, function(values, key) {
                            if (value.id == values.id) {
                                value.quantity = values.quantity;
                                values.stock = value.stock;
                                $scope.total += (value.quantity * parseInt(value.price));
                            }
                        })
                        $scope.productlist = result.data;
                    })
                    localStorage.setItem('water_cart', JSON.stringify(cart_data));
                } else {
                    $cordovaToast.showShortBottom(result.text);
                }
            }, function(error) {
                if (error == 500 || error == 404 || error == 0) {

                    $cordovaToast.showShortBottom(error);
                }
            });
        }
    }
    cart_list();
    $scope.sub = function(i) {
        if (i.quantity >> 1) {
            i.quantity--;
            cart_qty(i.id, i.quantity);
        }
         else
    {
        $scope.delete(i);
    }
    }
    $scope.add = function(i) {
        if (i.quantity < i.stock) {
            i.quantity++;
        } else {
            $cordovaToast.showShortBottom('required quantity higher than available stock');
        }
        if (i.quantity <= i.stock) {
            cart_qty(i.id, i.quantity);
        }
    }
    $scope.delete = function(list) {
        angular.forEach(cart_data, function(value, key) {
            if (value.id == list.id) {
                cart_data.splice(key, 1);
                $scope.total -= (value.quantity * value.price);
            }
        })
        if (cart_data.length > 0) {
            localStorage.setItem("water_cart", JSON.stringify(cart_data));
        } else {
            localStorage.removeItem("water_cart");
        }
        $cordovaToast.showShortBottom('Product deleted successfully from your cart');
        cart_list();
        count_cart();
    }
    cart_qty = function(id, qty) {
        $scope.total = 0;
        cart_data.forEach(function(value) {
            if (value.id == id) {
                value.quantity = qty;
            }
            $scope.total += (value.quantity * value.price)
        })
        localStorage.setItem("water_cart", JSON.stringify(cart_data));
    }
    $scope.checkout = function() {
        if (app_user != null) {
            check_product = [];
            cart_data.forEach(function(value, key) {
                console.log(value)
                if (value.stock <= 0) {
                    check_product.push(value);
                }
            })
            if (check_product.length == 0) {
                data = {};
                data.total = $scope.total;
                data.cart_data = cart_data;
                $state.go('app.checkout', { data: data });
            } else {
                $cordovaToast.showShortBottom("Some of your products out of stock. Please Check your cart");
            }
        } else {
            $cordovaToast.showShortBottom('login to checkout');
            $state.go('app.login');
        }
    }
})

.controller('checkoutCtrl', function($scope, $stateParams, service, $state, $cordovaToast, $ionicHistory, $ionicLoading) {
    $scope.curency = curency;
    $scope.actual_total = $stateParams.data.total;
    $scope.total = $scope.actual_total - ($scope.offer * $scope.actual_total / 100);
    cart_data = $stateParams.data.cart_data;
    checkout_data = {};
    checkout_data.products = cart_data;
    checkout_data.user_id = app_user.id;
    checkout_data.token = token_new;
    service.post(url_new + 'checkout', checkout_data).then(function(result) {
        if (result.status == "success") {
            $scope.common_offer = result.data.user_offer_amount;
            $scope.product_offer = result.data.product_offer;
            $scope.tax = result.data.tax;
            $scope.final_amount = result.data.final_amount;
            $scope.total = result.data.final_amount + (result.data.final_amount * $scope.tax / 100);
            $scope.actual_total = result.data.actual_total;
        }
    })
    service.post(url_new + 'city_list', {token:token}).then(function(res) {
      $scope.cities = res.data;
  })
    address_list = function() {
        data = {};
        data.token = token;
        data.page = 'users_address';
        data.user_id = app_user.id;
        service.post(url, data).then(function(result) {
            $scope.explaining = result.data;
        })
    }
    address_list();
    $scope.getorder = function(deliver_data) {
        deliver_data.products = $stateParams.data.cart_data;
        deliver_data.total = $scope.total;
        $state.go('app.payment', { data: deliver_data });
    }
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
})

.controller('paymentCtrl', function($scope, $timeout, $stateParams, $ionicLoading, service, $cordovaToast, $state, $ionicHistory, $cordovaInAppBrowser, $rootScope) {
    payment_data = $stateParams.data;
    payment_data.final_amount = payment_data.total;
    $scope.order = function(deliver_data) {
        $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
        service.post(url, deliver_data).then(function(result) {
            console.log(result);
            $ionicLoading.hide();
            if (result.status == 'success') {
                var obj_noti = {};
                obj_noti.user_id = app_user.id;
                obj_noti.message = "Order has been received successfully.";
                obj_noti.title = "Order Received";
                obj_noti.open_url = "my_order";
                service.post(url_new + "send_notification", obj_noti).then(function(res) {
                    service.post(res.url, res.params).then(function(responce) {
                    });
                })
                localStorage.removeItem('water_cart');
                $scope.deliver_data = {};
                $cordovaToast.showLongBottom('Order Placed Successfully');
                $state.go('app.home_page');
                $ionicHistory.removeBackView();
            } else {
                if (deliver_data.payment_type == 'WEBXPAY') {
                    $cordovaToast.showShortBottom('payment Failed');
                } else {
                    $state.go('app.cart');
                    $cordovaToast.showShortBottom(result.text);
                }
            }
        })
    }
    function success_callback(deliver_data) {
        $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
        service.post(url_new + 'get_responce', { xpay_id: xpay_id }).then(function(result) {
            $ionicLoading.hide();
            if (result.data.webxpay_status_code == 00) {
                deliver_data.token = token;
                $scope.order(deliver_data);
            } else {
                $cordovaToast.showShortBottom('payment Failed');
            }
        })
    }
    $scope.place_order = function(data) {
        console.log(data)
        deliver_data = payment_data;
        deliver_data.page = 'orders';
        deliver_data.customer_id = app_user.id;
        deliver_data.payment_type = data.type;
        deliver_data.credit_days = parseInt(data.days);
        if (!deliver_data.payment_type) {
            deliver_data.payment_type = '';
        }
        if (!deliver_data.credit_days) {
            deliver_data.credit_days = '';
        }
        if (deliver_data.payment_type == 'WEBXPAY') {
            deliver_data.token = token_new;
            service.post(url_new + 'make_transaction', deliver_data).then(function(result) {
                if (result.status == 'success') {
                    xpay_id = result.xpay_id;
                    var options = {
                        location: 'yes',
                        clearcache: 'yes',
                        toolbar: 'no'
                    };
                    document.addEventListener("deviceready", function() {
                        $cordovaInAppBrowser.open('http://water4u.lk/webxpay/index.php?xpay_id=' + xpay_id, '_blank', options)
                        .then(function(event) {
                            console.log(event);
                        })
                        .catch(function(event) {});

                        $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
                            var splitted = [];
                            var str = '';
                            str = event.url;
                            splitted = str.split("/");
                            for (var i = 0; i < splitted.length; i++) {
                                if (splitted[i] == "xpay_responce") {
                                    $timeout(function() {
                                        $cordovaInAppBrowser.close();
                                        success_callback(deliver_data);
                                    }, 2000);
                                }
                            }
                        });
                    }, false);
                }
            })
        } else {
            deliver_data.token = token;
            $scope.order(deliver_data);
        }
        count_cart();
    }
})

.controller('MyProfileCtrl', function($scope, $stateParams, service, $state, $cordovaToast, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,
    $ionicPopup, $ionicActionSheet, $ionicLoading) {
    $scope.appUser = app_user;
    $scope.edit_pro = true;
    $scope.edit_prof = function() {
        $scope.edit_pro = false;
    }
    $scope.update = function(app_user_data) {
        app_user_data.token = token;
        app_user_data.page = 'user_update';
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
        })
        if ($scope.model_image) {
            var targetPath = $scope.pathForImage($scope.model_image);
            app_user_data.image = $scope.model_image;
            var options = {
                fileKey: "image",
                fileName: $scope.model_image,
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params: app_user_data
            };
            $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
                $ionicLoading.hide();
                data = JSON.parse(result.response);
                app_user = data.data;
                localStorage.setItem('water_app_user', JSON.stringify(app_user));
                $cordovaToast.showShortBottom('Profile updated successfully');
                $state.go('app.home_page');
            }, function(error) {
                $ionicLoading.hide();
                $cordovaToast.showShortBottom("error" + JSON.stringify(error));

            });
        } else {
            service.post(url, app_user_data).then(function(result) {
                console.log(result)
                $ionicLoading.hide();
                localStorage.setItem('water_app_user', JSON.stringify(app_user_data));
                $cordovaToast.showShortBottom('Profile updated successfully');
                $state.go('app.home_page');
            }, function(error) {
                $ionicLoading.hide();
                $cordovaToast.showShortBottom("error" + JSON.stringify(error));
            });
        }
    }
    $scope.loadImage = function() {
        var showActionSheet = $ionicActionSheet.show({
            buttons: [{
                text: 'Load from Library'
            },
            {
                text: 'Use Camera'
            }
            ],
            titleText: 'Select Image Source',
            cancelText: 'Cancel',
            cancel: function() {
            },
            buttonClicked: function(index) {
                var type = null;
                if (index === 0) {
                    type = Camera.PictureSourceType.PHOTOLIBRARY;
                }
                if (index === 1) {
                    type = Camera.PictureSourceType.CAMERA;
                }
                if (type !== null) {
                    $scope.selectPicture(type);
                }
                return true;
            },

            destructiveButtonClicked: function() {
            }
        });
    };
    $scope.pathForImage = function(image) {
        if (image === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + image;
        }
    };
    var temp = {};
    $scope.selectPicture = function(sourceType) {
        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imagePath) {
            var currentName = imagePath.replace(/^.*[\\\/]/, '');
            var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
                window.FilePath.resolveNativePath(imagePath, function(entry) {
                    window.resolveLocalFileSystemURL(entry, success, fail);
                    function fail(e) {
                        $cordovaToast.showShortBottom('Error: 9', e);
                    }
                    function success(fileEntry) {
                        var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                        $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success) {
                            $scope.model_image = newFileName;
                        }, function(error) {
                            $cordovaToast.showShortBottom('Error : ' + error.exception);
                        });
                    };
                });
            } else {
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success) {
                    $scope.model_image = newFileName;
                }, function(error) {
                    $cordovaToast.showShortBottom('Error : ' + error.exception);
                });
            }
        },
        function(err) {
            $cordovaToast.showShortBottom('Error :12');
        })
    };
    $scope.camera_open = function($image) {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var image = document.getElementById('myImage');
            image.src = "data:image/jpeg;base64," + imageData;
        }, function(err) {
        });
    };
    $scope.post_enquiry = function(data) {
        data.token = token;
        data.page = 'post_enquiry';
        data.user_id = app_user.id;
        service.post(url, data).then(function(result) {
            if (result.status == 'success') {
                $cordovaToast.showShortBottom(result.message);
                enq();
                $state.go('app.home_page');
            }
        })
    }
    enq = function() {
        obj = {};
        obj.token = token;
        obj.page = 'enquiry_details';
        obj.user_id = app_user.id;
        service.post(url, obj).then(function(result) {
            if (result.status == 'success') {
                $scope.enquires = result.data;
            }
        })
    }
    enq();
})

.controller('MyOfferCtrl', function($scope, $stateParams, service, $ionicLoading) {
    $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
    var user = JSON.parse(localStorage.getItem('water_app_user'));
    var obj = { 'token': token, 'page': 'get_my_offer', 'user_id': user.id }
    service.post(url, obj).then(function(result) {
        console.log(result)
        $ionicLoading.hide();
        if (result.status == 'success') {
            if (result.data.product_offers != undefined) {
                if (result.data.product_offers != 0) {
                    $scope.offers = result.data;
                }
            } else {
                $scope.offers = '';
            }
            if (result.data.user_offer != undefined) {
                $scope.user_offer = result.data.user_offer.offer;
                app_user.offer = result.data.user_offer;
            } else {
                $scope.user_offer = '';
            }
        }
    });
})

.controller('MyOrderCtrl', function($scope, $stateParams, service, $cordovaToast, $filter, $ionicLoading) {
    $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
    obj = {};
    obj.token = token;
    obj.user_id = app_user.id;
    obj.page = "user_orders";
    service.post(url, obj).then(function(result) {
        $ionicLoading.hide();
        if (result.status == 'success') {
            angular.forEach(result.data, function(value, key) {
              value.created_at = $filter('date')(new Date(value.created_at.replace(/-/g,"/")), 'dd-MM-yyyy');
          })
            $scope.userorderlists = result.data;
        }
    });
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
})

.controller('forgotPasswordCtrl', function($scope, $stateParams, $cordovaToast, $state, service) {
    $scope.step1 = true;
    user_id = '';
    email = '';
    $scope.getotp = function(data) {
        data.token = token;
        data.page = 'send_otp';
        data.email = data.email.toLowerCase();
        email = data.email;
        this.data = {};
        service.post(url, data).then(function(result) {
            if (result.status == 'success') {
                otp = result.data.otp;
                email = result.data.email;
                service.post(url_new + 'send_mail', { otp: otp, email: email }).then(function(res) {
                    $cordovaToast.showShortBottom(result.message);
                    $scope.step1 = false;
                    $scope.step2 = true;
                })
            } else {
                $cordovaToast.showShortBottom(result.message);
            }
        });
    };
    $scope.resend_otp = function() {
        data = {};
        data.token = token;
        data.email = email;
        this.data = {};
        service.post(url, data).then(function(result) {
            $cordovaToast.showShortBottom(result.message);
            if (result.status == 'success') {
                user_id = result.data.user_id;
                $scope.step1 = false;
                $scope.step2 = true;
            }
        });
    };
    $scope.otp_verification = function(data) {
        data.token = token;
        data.page = 'verify_otp';
        data.email = email;
        this.data = {};
        service.post(url, data).then(function(result) {
            if (result.status == 'success') {
                $scope.step1 = false;
                $scope.step2 = false;
                $scope.step3 = true;
            } else {
                $cordovaToast.showShortBottom(result.message);
            }
        });
    };
    $scope.change_password = function(data) {
        data.token = token;
        data.page = 'change_password';
        data.email = email;
        this.data = {};
        if (data.new_password == data.re_password) {
            service.post(url, data).then(function(result) {
                if (result.status == 'success') {
                    $cordovaToast.showShortBottom('password changed successfully');

                    $state.go('app.login');
                }
                else
                {
                    $cordovaToast.showShortBottom(result.text);
                }
            });
        } else {
            $cordovaToast.showShortBottom('password mismatched');
        }
    };
})

.controller('delivery_mapCtrl', function($scope, $state, Map, $stateParams, $cordovaToast, $cordovaGeolocation, $interval) {
    Map.init(13.0326624, 80.26710460000004);
    address = JSON.parse($stateParams.address);
    var posOptions = { timeout: 50000, enableHighAccuracy: true };
    $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position) {
        console.log(position);
        var lat = position.coords.latitude
        var lng = position.coords.longitude
        data = {};
        data.lat = lat;
        data.lng = lng;
        data.timestamp = position.timestamp;
        Map.direction([], data, { lat: address.latitude, lng: address.longitude });
    }, function(err) {
        console.log(err);
    });
})

.controller('delivery_boyCtrl', function($scope, $state, Map, $stateParams, $cordovaToast, $cordovaGeolocation, $ionicLoading, $rootScope, $interval, service, $filter) {
    var interval = '';
    order_list = function() {
        $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
        obj = {};
        obj.token = token_new;
        obj.user_id = app_user.id;
        service.post(url_new + 'order', obj).then(function(result) {
            console.log(result.data);
            $ionicLoading.hide();
            if (result.status == "success") {
                $scope.orders_list = result.data;
            }
        })
    }
    order_history = function() {
        $ionicLoading.show({ template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>' })
        obj = {};
        obj.token = token_new;
        obj.user_id = app_user.id;
        service.post(url_new + 'order_history', obj).then(function(result) {
            $ionicLoading.hide();
            if (result.status == "success") {
                $scope.orders_history = result.data;
                console.log($scope.orders_history);
            }
        })
    }
    if (app_user.attendance_status == 1) {
        $scope.status = true;
        order_list();
    } else {
        $scope.status = false;
    }
    cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
        if (canRequest) {
            cordova.plugins.locationAccuracy.request(function(success) {}, function(error) {
                if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {
                    if (window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")) {}
                }
        }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
        } else {
        }
    });
    $scope.active = function(status) {
        if (status == true) {
            $scope.status = true;
            status = 1;
            $rootScope.check_track($scope.status);
        } else {
            $scope.status = false;
            status = 0;
            if (angular.isDefined(interval)) {
                $interval.cancel(interval);
                interval = undefined;
            }
        }
        obj = {};
        obj.token = token_new;
        obj.user_id = app_user.id;
        obj.status = status;
        service.post(url_new + 'delivery_boy_status', obj).then(function(result) {
            app_user = result.data;
            localStorage.setItem('water_app_user', JSON.stringify(result.data));
            if (result.status == "success" && status == 1) {
                order_list();
            } else {
                $cordovaToast.showShortBottom(result.message);
                $scope.status = false;
            }
        })
    }
    order_history();
    track = function() {
        if ($scope.status == true) {
            var posOptions = { timeout: 5000, enableHighAccuracy: true };
            $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position) {
                var lat = position.coords.latitude
                var lng = position.coords.longitude
                data = {};
                data.lat = lat;
                data.lng = lng;
                data.timestamp = position.timestamp;
                firebase.database().ref('water4u/' + app_user.unique_id + '/watch').push(data).then(function(res) {
                    console.log('watch');
                });
                firebase.database().ref('water4u/' + app_user.unique_id + '/current').set(data).then(function() {
                    console.log('current');
                });
            }, function(err) {
                console.log(err);
            });
        }
    }
    $rootScope.check_track($scope.status);
})

.controller('order_detailsCtrl', function($scope, $state, Map, $stateParams, $cordovaToast, $interval, service, $filter) {
    obj = {};
    obj.token = token_new;
    obj.order_id = $stateParams.order_id;
    service.post(url_new + 'order_detail', obj).then(function(result) {
        $scope.order_detail = result.data;
    })
    $scope.deliver = function(order) {
        obj = {};
        obj.token = token_new;
        obj.delivery_id = 1;
        obj.order_id = order.id;
        service.post(url_new + 'delivery_detail', obj).then(function(result) {
            if (result.status == 'success') {
                $cordovaToast.showShortBottom('delivery status changed');
                $state.go('app.delivery_order_list');
            }
        })
    }
});
